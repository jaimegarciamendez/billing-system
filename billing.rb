require 'byebug'
require 'date'
require 'time'
require './data/calls'
require './modules/phonecall'
require 'faker'

class Billing
  include PhoneCall

  MONTHLY_PAYMENT = 500

  def initialize(data, month)
    @client = data[0]
    @calls = data[1]
    @month = month
    @total_calls = 0
    @error = nil
    @detailed_calls = []
  end

  def monthly_invoice
    validations
    return if @error
    
    calculate_bill
    print_invoice
  end

  private

  def validations
   valid_month?
   month_phone_calls? unless @error
  end

  def valid_month?
    valid_months = Date::ABBR_MONTHNAMES.compact.map(&:downcase)
    puts @error = "Ops! Invalid month. Please, try another" unless valid_months.include? @month
  end

  def month_phone_calls?
    found = @calls.any? { |call| Time.parse(call[:start]).strftime("%b").downcase == @month }
    puts @error = "Ops! No phone calls made that month" unless found
  end

  def calculate_bill
    @calls.each do |call|
      @detailed_calls << cost_by_type(call)
    end
  end

  def print_invoice
    local_minutes, local_amount = local_calls_info(@detailed_calls)
    natio_minutes, natio_amount = natio_calls_info(@detailed_calls)
    inter_minutes, inter_amount = inter_calls_info(@detailed_calls)
    total_minutes, total_amount = every_calls_info(@detailed_calls)

    puts "***********************************************************************************************"
    puts "Invoice #{Faker::Invoice.creditor_reference}- Client ID #{@client[:id].upcase} - #{@client[:full_name]}"
    puts "Address: #{@client[:address]}"
    puts "***********************************************************************************************"
    puts "Local phone calls: --> minutes: #{local_minutes} - amount: $ #{sprintf('%.2f', local_amount)}"
    puts "National phone calls: --> minutes: #{natio_minutes} - amount: $ #{sprintf('%.2f', natio_amount)}"
    puts "International phone calls --> minutes: #{inter_minutes} - amount: $ #{sprintf('%.2f', inter_amount)}"
    puts "***********************************************************************************************"
    puts "***********************************************************************************************"
    puts "Total minutes: #{total_minutes} - Total Amount: $ #{sprintf('%.2f', total_amount)}"
  end

  def local_calls_info(detailed_calls)
    local = detailed_calls.select {|call| call[:type] == "Local"}
    minutes = local.map { |call| call[:duration] }.sum
    amount = local.map { |call| call[:total_cost] }.sum
    [minutes, amount]
  end

  def natio_calls_info(detailed_calls)
    natio = detailed_calls.select {|call| call[:type] == "National"}
    minutes = natio.map { |call| call[:duration] }.sum
    amount = natio.map { |call| call[:total_cost] }.sum
    [minutes, amount]
  end

  def inter_calls_info(detailed_calls)
    inter = detailed_calls.select {|call| call[:type] == "International"}
    minutes = inter.map { |call| call[:duration] }.sum
    amount = inter.map { |call| call[:total_cost] }.sum
    [minutes, amount]
  end

  def every_calls_info(detailed_calls)
    minutes = detailed_calls.map { |call| call[:duration] }.sum
    amount = detailed_calls.map { |call| call[:total_cost] }.sum
    [minutes, amount]
  end
end

puts "Welcome to our Phone Calls Billing System"
print "Please, enter a month [jan feb mar apr may jun jul aug sep oct nov dec]: "
month_input = gets.chomp
month = month_input[0..2]
puts "Calculating invoice..."
sleep(2)
fake_data = Calls.new.fake_data
billing = Billing.new(fake_data, month&.downcase)
billing.monthly_invoice

