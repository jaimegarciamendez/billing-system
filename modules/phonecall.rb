module PhoneCall

  BUSINESS_DAYS = %w[Monday Tuesday Wednesday Thursday Friday]
  BASIC_COST = 0.2

  def cost_by_type(call)
    minutes = (Time.parse(call[:end]) - Time.parse(call[:start]))/60
    total_cost = calculate_cost(call, minutes)
    { duration: minutes.to_i, total_cost: total_cost.round(2), type: "#{call_type(call)}", zone: "#{call[:type][2]}" }
  end

  private

  def calculate_cost(call, minutes)
    case call[:type][0]
    when 'L' then price_for_local_call(call) * minutes
    when 'N' then price_for_national_call(call) * minutes
    when 'I' then price_for_international_call(call) * minutes
    end
  end

  def price_for_local_call(call)
    return BASIC_COST if BUSINESS_DAYS.include? dayname(call) && hour(call).between?(8,20)

    BASIC_COST / 2
  end

  def price_for_national_call(call)
    BASIC_COST * call[:type][2].to_i
  end

  def price_for_international_call(call)
    BASIC_COST * call[:type][2].to_i * 1.2
  end

  def dayname(call)
    Time.parse(call[:start]).strftime('%A')
  end

  def hour(call)
    Time.parse(call[:start]).strftime('%H').to_i
  end
  
  def call_type(call)
    return 'Local'          if call[:type][0] == 'L'
    return 'National'       if call[:type][0] == 'N'
    return 'International'  if call[:type][0] == 'I'
  end
end