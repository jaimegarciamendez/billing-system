# BILLING SYSTEM

Paso a paso

- `git clone https://gitlab.com/jaimegarciamendez/billing-system.git`
- cd billing-system
- `ruby billing.rb`


# ACLARACIONES

<p>En los datos inventados de llamadas, se usa el campo "type" que está compuesto por una letra y un número, ejemplo: "L-1".</p>
<p>La letra determina si la llamada es Local, Nacional o Internacional --> L, N, I</p>
<p>El número determina la zona a donde fue realizada la llamada --> 1, 2, 3, 4, 5, 6 ... N</p>

Se eligió esto para mayor comodidad y simpleza a la hora de realizar los cálculos de los costos de las llamadas, ya que no estaba determinado específicamente cuales eran los costos de las llamadas nacionales e internacionales.

Teniendo en cuenta que el costo básico es 0.2 centavos, los costos de las llamadas se componen de la siguiente forma:
<p></p>
<p></p>

**- Llamadas Locales:**
<p>Lunes a viernes de 8 a 20 hs --> COSTO BASICO (0.2 centavos)</p>
<p>Lo demás, incluídos sábados y domingos --> COSTO BASICO / 2 (0.1 centavos)</p>
<p></p>
<p></p>

**- Llamadas Nacionales:**
<p>Se calculan multiplicando el COSTO BASICO por el número de la zona:</p>
<p>N-1 --> COSTO BASICO * 1 (0.2 centavos)</p>
<p>N-2 --> COSTO BASICO * 2 (0.4 centavos)</p>
<p>N-3 --> COSTO BASICO * 3 (0.6 centavos)</p>
<p>N-4 --> COSTO BASICO * 4 (0.8 centavos)</p>
<p>Y así sucesivamente</p>
<p></p>
<p></p>

**- Llamadas Internacionales:**
<p>Se calculan multiplicando el COSTO BASICO por el número de la zona y se agrega un 20%:</p>
<p>I-1 --> COSTO BASICO * 1 * 1.2 (0.24 centavos)</p>
<p>I-2 --> COSTO BASICO * 2 * 1.2 (0.48 centavos)</p>
<p>I-3 --> COSTO BASICO * 3 * 1.2 (0.72 centavos)</p>
<p>I-4 --> COSTO BASICO * 4 * 1.2 (0.96 centavos)</p>
<p>Y así sucesivamente</p>

